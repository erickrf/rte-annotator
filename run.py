# -*- coding: utf-8 -*-

from rteannotator import app

if __name__ == '__main__':
    app.run(debug=True, threaded=True)

