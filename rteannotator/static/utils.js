function removeClass(object, cssClass){
    re = new RegExp('(?:^|\\s)' + cssClass + '(?!\\S)', 'g');
    object.className = object.className.replace(re, '' )
}