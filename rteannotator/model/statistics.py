# -*- coding: utf-8 -*-

'''
Functions to extract statistics from the data.
'''

import numpy as np
import scipy.stats as stats
import scipy.sparse as sparse
from sqlalchemy import func
from collections import Counter

from rteannotator import db
from rteannotator.model import data, utils

def compute_overall_statistics(annotator_ids=None):
    '''
    Compute the same set of statistics as `compute_file_statistics`. It consider
    all pairs from all files marked as available.
    
    :param annotator_ids: if given, only consider annotations by these annotators. 
    '''
    Pair = data.Pair
    File = data.File
    Annotation = data.Annotation
    
    # get the id's of all pairs with the sufficient number of annotations
    qid = db.session.query(Pair.id).join(File).join(Annotation)
    qid = qid.filter(Annotation.similarity != None,
                     Annotation.entailment != None,
                     Pair.reviewed == True,
                     Pair.flag_delete == False,
                     File.available == True)
    
    if annotator_ids is not None:
        qid = qid.filter(Annotation.annotator_id.in_(annotator_ids))
    
    qid = qid.group_by(Pair.id).having(func.count(Annotation.annotator_id) == data.DESIRED_ANNOTATIONS_PER_PAIR)
        
    entailment_matrix = get_entailment_matrix(qid)
    similarity_matrix = get_similarity_matrix(qid)
    
    stats = _compute_statistics(entailment_matrix, similarity_matrix)
    
    return stats

def _compute_agreement_for_pair(this_annotator_entailment, other_annotators_entailment):
    '''
    Internal helper function. Return the agreement as a rational number.
    
    :param this_annotator_entailment: int
    :param other_annotators_entailment: list of ints
    '''
    num_same_choices = other_annotators_entailment.count(this_annotator_entailment)
    return float(num_same_choices) / len(other_annotators_entailment)    
    
def compute_annotators_statistics(annotator_ids):
    '''
    Compute the individual mean agreements of a group of annotators. These values
    show how much they agreed with other annotators on the same pairs.
    '''
    # query for all pair id's this annotator annotated
    annotations = utils.get_all_valid_annotations()
    annotations_per_pair = Counter(ann[1] for ann in annotations)
    pairs_with_enough_annotations = set(id_ for id_ in annotations_per_pair 
                                        if annotations_per_pair[id_] == data.DESIRED_ANNOTATIONS_PER_PAIR)
    
    agreement_means = []
    for annotator_id in annotator_ids:
        annotator_id = int(annotator_id)
        ids_by_annotator = set(ann[1] for ann in annotations if ann[0] == annotator_id)
        valid_pair_ids = ids_by_annotator.intersection(pairs_with_enough_annotations)
        
        pair_being_checked = None
        other_annotators_entailment = []
        this_annotator_entailment = None
        
        # we need to create one array for each annotator because they may have annotated
        # different numbers of pairs
        agreements = np.empty(len(valid_pair_ids))
        idx = 0
        
        if len(valid_pair_ids) == 0:
            agreement_means.append(0)
            continue
        
        for annotation in annotations:
            pair_id = annotation[1]
            if pair_id not in valid_pair_ids:
                continue
            
            if pair_id != pair_being_checked:
                if pair_being_checked is not None:
                    agreement = _compute_agreement_for_pair(this_annotator_entailment, 
                                                            other_annotators_entailment)
                    agreements[idx] = agreement
                    idx += 1
                    this_annotator_entailment = None
                    other_annotators_entailment = []
                
                pair_being_checked = pair_id
                
            entailment_value = annotation[2]
            if annotation[0] == annotator_id:
                this_annotator_entailment = entailment_value
            else:
                other_annotators_entailment.append(entailment_value)
        
        agreement = _compute_agreement_for_pair(this_annotator_entailment, other_annotators_entailment)
        agreements[-1] = agreement
        agreement_means.append(agreements.mean())
    
    return agreement_means
    

def compute_file_statistics(file_id):
    '''
    Compute a set of statistics for the file with the given id. Only consider pairs
    with enough annotations.
    
    Return a list of tuples.
    '''
    # get annotations on all pairs with enough ones
    Pair = data.Pair
    qid = utils.get_annotated_pairs_query(file_id, [Pair.id])
    
    entailment_matrix = get_entailment_matrix(qid)
    similarity_matrix = get_similarity_matrix(qid)
    stats = _compute_statistics(entailment_matrix, similarity_matrix)
    
    return stats

def _compute_statistics(entailment_matrix, similarity_matrix):
    '''
    Internal helper function
    Entailment matrix format: (pair_id, entailment class, count)
    Similarity matrix format: (pair_id, annotator_id, similarity)
    '''
    normalize_ids(entailment_matrix, 0)
    agreement_metrics = compute_kappa(entailment_matrix)
    
    normalize_ids(similarity_matrix, 0)
    normalize_ids(similarity_matrix, 1)
    correlation_metrics, similarity_averages = compute_correlation(similarity_matrix)
    
    # now, get the pairs with a majority vote in an entailment class
    threshold = data.DESIRED_ANNOTATIONS_PER_PAIR / 2 + 1
    inds = entailment_matrix.T[2] >= threshold
    entailment_with_majority = entailment_matrix[inds]
    entailment_counter = Counter(entailment_with_majority.T[1])
    
    # the similarity matrix has been normalized for pair_id, so they go from 0 to len - 1
    ids_with_majority = entailment_with_majority.T[0]
    similarities_with_majority = similarity_averages[ids_with_majority]
    
    similarity_counter = {}
    bin_limits = [1.0, 1.9, 2.9, 3.9, 4.9, 5.0]
    similarity_bins = np.histogram(similarities_with_majority, bins=bin_limits)[0]
    for i, count in enumerate(similarity_bins):
        min_value = bin_limits[i]
        max_value = bin_limits[i + 1]
        key = '{} - {}'.format(min_value, max_value)
        similarity_counter[key] = count
    
    metrics = []
    for key in correlation_metrics:
        value = '{:0.3f}'.format(correlation_metrics[key])
        metrics.append((key, 'Similarity', value))
    
    for key in agreement_metrics:
        value = '{:0.3f}'.format(agreement_metrics[key])
        metrics.append((key, 'Entailment', value))
    
    num_annotations = len(similarity_matrix)
    s = 'Pairs with %d annotations' % data.DESIRED_ANNOTATIONS_PER_PAIR
    
    metrics.append((s, 'Both', num_annotations / 4))
    metrics.append(('Pairs with majority vote', 'Entailment', len(entailment_with_majority)))
    metrics.append(('Total annotations', 'Both', num_annotations))
    
    named_entailment_counter = {}
    for key in entailment_counter:
        name = data.Annotation.value_map[key + 1]
        named_entailment_counter[name] = entailment_counter[key]
    
    return (metrics, named_entailment_counter, similarity_counter)

def get_similarity_matrix(qid):
    '''
    Rows: (pair_id, annotator_id, similarity)
    
    :param qid: query for pair ids to include.
    '''
    Annotation = data.Annotation
    q_similarity = db.session.query(Annotation.pair_id, Annotation.annotator_id, Annotation.similarity)
    q_similarity = q_similarity.filter(Annotation.pair_id.in_(qid))
    
    similarity_data = q_similarity.all()
    similarity_matrix = np.array(similarity_data)
    
    return similarity_matrix

def get_entailment_matrix(qid):
    '''
    Return a matrix with rows (pair_id, entailment class, count)
    
    :param qid: query for pair ids to include
    '''
    Annotation = data.Annotation
    q_entailment = db.session.query(Annotation.pair_id, Annotation.entailment, func.count(Annotation.annotator_id))
    q_entailment = q_entailment.filter(Annotation.pair_id.in_(qid)).group_by(Annotation.pair_id, Annotation.entailment)
    entailment_data = q_entailment.all()
    entailment_count_matrix = np.array(entailment_data)
    
    return entailment_count_matrix

def compute_correlation(similarity_matrix):
    '''
    Matrix rows: (pair_id, annotator_id, similarity)
    
    Id's should have already been normalized
    '''
    values = {}
    if len(similarity_matrix) == 0:
        return (values, None)
    
    # create the matrix as a sparse one because it is far easier
    # then convert it to dense
    rows = similarity_matrix.T[0]
    cols = similarity_matrix.T[1]
    sims = similarity_matrix.T[2]
    m = sparse.coo_matrix((sims, (rows, cols)), dtype=np.float)
    scores = m.todense()
        
    # todense() creates a np.matrix. arrays are easier to work with
    scores = np.array(scores)
    
    # nan means the annotator didn't annotate that pair
    scores[scores == 0] = np.nan
    
    standard_devs = np.nanstd(scores, 1)
    values['Averaged SD'] = standard_devs.mean()
    
    num_annotators = scores.shape[1]
    correlations = [annotator_pearson(scores, annotator)
                    for annotator in range(num_annotators)]
    
    # get the total number of annotators given by each annotator
    annotations_by_annotator = Counter(cols)
    
    # order the weights by normalized id
    weights = [annotations_by_annotator[id_] for id_ in range(num_annotators)]
    
    correlations = np.array(correlations)
    pair_avg_similarity = np.nanmean(scores, 1)
    
    values['Pearson corr.'] = np.average(correlations, weights=weights)
    return (values, pair_avg_similarity)
    
def normalize_ids(count_matrix, column_num):
    '''
    Normalize a column in a count matrix. It maps arbitrary IDs so that
    it has values starting from 0 and no gaps. Original IDs are not accessible
    anymore.
    '''
    # pair id's may be non-continuous (i.e., we have pair 1, 3, 4, 6 etc)
    # make it continuous so the count matrix doesn't consider more pairs than what actually exists
    ids = count_matrix.T[column_num]
    sorted_ids = sorted(np.unique(ids))
    mapping = {id_: x for (id_, x) in zip(sorted_ids, range(len(sorted_ids)))}
    
    for id_ in sorted_ids:
        # follow a sorted order to avoid overwriting changes! 
        # (ids are mapped to a value equal or lesser)
        count_matrix.T[column_num][ids == id_] = mapping[id_]
    

def compute_kappa(count_matrix):
    '''
    Compute the Fleiss's Kappa interannotator agreement on entailment annotation. 
    
    Each row in count_matrix: (file_id, entailment_code, count)
    
    Return (agreement %, kappa)
    '''
    values = {}
    
    if len(count_matrix) == 0:
        return values
    
    # entailment codes start from 1, subtract 1 so they start from 0
    # (this is only for array indexing purposes, we won't need the actual values here)
    count_matrix.T[1] -= 1
    
    # get a sparse matrix of counts
    data_ = count_matrix.T[2]
    coords = count_matrix.T[:2]
    m = sparse.coo_matrix((data_, coords)).tocsr()
    
    # get the array of agreement rates per pair (p)
    m2 = m.copy()
    m2.data **= 2
    n = data.DESIRED_ANNOTATIONS_PER_PAIR
    p = (m2.sum(1) - n) / (n * (n - 1.0))
    p_mean = p.mean()
    
    # the probability expected by chance equals the sum of the squares of all 
    # class proportions
    total_annotations = m.sum()
    # convert matrix to array
    count_per_class = np.array(m.sum(0))[0]
    proportion_per_class = count_per_class / float(total_annotations)
    p_expected = np.sum(proportion_per_class ** 2)
    
    kappa = (p_mean - p_expected) / (1.0 - p_expected)
    values['Kappa'] = kappa
    values['Agreement'] = p_mean
     
    return values

def annotator_pearson(scores, annotator):
    '''
    Compute the Pearson correlation of a single annotator with the others. 
    
    Scores should be a 2-dim numpy array in the format (num_pairs, num_annotators).
    Pairs that an annotator didn't rate should be nan.
    '''
    # find the indices of the pairs this annotator annotated
    inds_annotated = ~np.isnan(scores[:, annotator])
    
    scores_by_user = scores[inds_annotated, annotator]
    
    # now take the scores by annotators with id's to the left and to the right
    scores_left = scores[inds_annotated, 0:annotator]
    scores_right = scores[inds_annotated, annotator + 1:]
    scores_by_others = np.hstack((scores_left, scores_right))
    averages_by_others = np.nanmean(scores_by_others, 1)
    
    return stats.stats.pearsonr(scores_by_user, averages_by_others)[0]
        

