# -*- coding: utf-8 -*-

'''
This module contains classes for the entities representing in the
database.
'''

from rteannotator import db
from flask_login import current_user

DESIRED_ANNOTATIONS_PER_PAIR = 4

class Annotator(db.Model):
    '''
    Class mapping to the Annotator in the DB.
    Represents one person that annotates the data.
    '''
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column(db.String(50), index=True, unique=True)
    password = db.Column(db.String(128))
    salt = db.Column(db.String(128))
    show_all_pairs = db.Column(db.Boolean, default=True)
    type = db.Column(db.Integer, default=1)
    
    # all individual annotations (i.e. annotated pairs) by this user 
    annotations = db.relationship('Annotation', backref='annotator',
                                  lazy='dynamic')
    
    user_types = {1: 'Normal',
                  2: 'Expert',
                  3: 'Admin'}
    
    def __repr__(self):
        return '<Annotator {}>'.format(self.name)
        
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False
    
    def may_submit_file(self):
        return self.type == 3
    
    def may_edit_pair(self):
        '''Only expert/admins can edit pairs'''
        return self.type > 1
    
    def may_download_raw_file(self):
        return self.type == 3
    
    def may_download_annotated_file(self):
        return self.type == 3
    
    def may_create_user(self):
        return self.type > 1
    
    def may_edit_user(self):
        return self.type == 3
    
    def may_see_all_statistics(self):
        return self.type > 1
    
    def may_edit_file(self):
        return self.type == 3
    
    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3
    
class File(db.Model):
    '''
    Class representing a file containing many TE examples.
    '''
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column(db.String(50), index=True, unique=True)
    vsm = db.Column(db.String(10))
    available = db.Column(db.Boolean, default=True)
    pairs = db.relationship('Pair', backref='file', lazy='dynamic')
    
    def __repr__(self):
        return '<File {}>'.format(self.name)
    
    def __init__(self, name, vsm=None):
        self.name = name
        self.vsm = vsm
    
    def load_metadata(self):
        '''
        Load some metadata to the File object. The data are meant to be 
        accessed by the templates, and include statistics over the annotation.
        '''
        from rteannotator.model import utils
        annotation_query = utils.get_file_annotation_query(self.id)
        annotations = annotation_query.all()
        
        self.num_annotations = len(annotations)
        self.num_annotated_by_user = len([a for a in annotations
                                          if a.annotator_id == current_user.id])
        
        # count pairs with enough annotations
        self.num_annotated_enough = utils.count_annotated_enough(annotations)
        
        # count reviewed pairs
        self.num_reviewed_and_kept = utils.count_reviewed_and_kept(self.id)
        self.num_reviewed_by_user = utils.count_reviewed_by_user(self.id)

class Pair(db.Model):
    '''
    A TE candidate pair and its associated metadata.
    '''
    id = db.Column(db.Integer, primary_key=True)
    id_in_file = db.Column(db.Integer, index=True)
    t = db.Column(db.Text)
    h = db.Column(db.Text)
    similarity = db.Column(db.Float)
    alpha1 = db.Column(db.Float)
    alpha2 = db.Column(db.Float)
    cluster = db.Column(db.String(50))
    file_id = db.Column(db.Integer, db.ForeignKey('file.id'), index=True)
    flag_delete = db.Column(db.Boolean, default=False)
    
    # signals if the pair has already been reviewed as appropriate for the corpus
    reviewed = db.Column(db.Boolean, default=False)
    
    # indicate who reviewed the pair
    reviewer_id = db.Column(db.Integer, db.ForeignKey('annotator.id'), index=True)
    
    # reviewed pairs are pairs checked for validity (i.e., edited if necessary, 
    # only kept if not noisy) but not actually annotated
    reviewer = db.relationship('Annotator', 
                               backref=db.backref('reviewed_pairs', lazy='dynamic'))
    
    # list of annotations objects: each one was an annotation instance
    # by a different user
    annotations = db.relationship('Annotation', backref='pair', lazy='dynamic',
                                  cascade='save-update, merge, delete, delete-orphan')
        
    def __repr__(self):
        return '<Pair {} in file {}>'.format(self.id, self.file_id)
    
    def __init__(self, xml_element=None, file_=None):
        '''
        :param xml_element: a Pair XML element from a file in the
            RTE challenge format. It should contain the t and h
            subelements, plus the attributes id, entailment and
            similarity.
        '''
        if xml_element is None:
            return
        self.t = xml_element.find('t').text
        self.h = xml_element.find('h').text
        self.attributes = xml_element.attrib   
        self.id_in_file = xml_element.get('id')
        self.file = file_
        for item in ['alpha1', 'alpha2', 'similarity', 'cluster']:
            setattr(self, item, xml_element.get(item))
    
class Annotation(db.Model):
    '''
    Class for storing how a given annotator annotate a given TE pair.
    '''
    annotator_id = db.Column(db.Integer, db.ForeignKey('annotator.id'), primary_key=True)
    pair_id = db.Column(db.Integer, db.ForeignKey('pair.id'), primary_key=True)
    entailment = db.Column(db.Integer, index=True)
    similarity = db.Column(db.Integer, index=True)
    
    value_map = {1: 'First entails second',
                 2: 'Second entails first',
                 3: 'Paraphrase',
                 4: 'None'}

class PendingAnnotation(db.Model):
    '''
    Class to store which pairs are assigned to which annotators to annotate.
    '''
    annotator_id = db.Column(db.Integer, db.ForeignKey('annotator.id'), primary_key=True)
    pair_id = db.Column(db.Integer, db.ForeignKey('pair.id'), primary_key=True)
    annotator = db.relationship('Annotator', backref='pending_annotations')
    pair = db.relationship('Pair', 
                           backref=db.backref('pending_annotations', 
                                              lazy='dynamic',
                                              cascade='all, delete-orphan'))
    
    def __repr__(self):
        return '<Pending Annotation by annotator %s on pair %s>' % (self.annotator_id,
                                                                    self.pair_id)    

class PendingReview(db.Model):
    '''
    Class to store which pairs are being evaluated (for deciding if it should be edited or
    discarded) by which users.
    '''
    annotator_id = db.Column(db.Integer, db.ForeignKey('annotator.id'))
    pair_id = db.Column(db.Integer, db.ForeignKey('pair.id'), primary_key=True)
    annotator = db.relationship('Annotator', backref='pending_review')
    pair = db.relationship('Pair', 
                           backref=db.backref('pending_review', 
                                              cascade='all, delete-orphan'))
    
    def __repr__(self):
        return '<Pending Review by annotator %s on pair %s>' % (self.annotator_id,
                                                                self.pair_id)

class ReviewCounter(db.Model):
    '''
    Count how many pairs an annotator reviewed in a file.
    
    This table is necessary instead of using a field for annotator in the pairs because
    deleted pairs should also be counted.
    '''
    annotator_id = db.Column(db.Integer, db.ForeignKey('annotator.id'), primary_key=True)
    file_id = db.Column(db.Integer, db.ForeignKey('file.id'), primary_key=True)
    count = db.Column(db.Integer)
    
    annotator = db.relationship('Annotator', 
                                backref=db.backref('review_counts', lazy='dynamic'))
    file = db.relationship('File', 
                           backref=db.backref('review_counts', lazy='dynamic'))
    
    def __repr__(self):
        return '<ReviewCounter for user %d on file %d: %d>' % (self.annotator_id,
                                                               self.file_id,
                                                               self.count)
    