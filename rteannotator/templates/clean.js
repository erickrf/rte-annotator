$(function() {
    // load T and H when the document is ready
    load_pair();
});

// vars to keep track of T and H change
var originalText1 = '';
var originalText2 = '';

function load_pair() {
    // retrieve a T and T pair via ajax and load their content into the page
    url = '{{ url_for("get_pair_for_review", file_id=file_id) }}';
    callback = function(response, status) {
        data = JSON.parse(response);
        if (data.empty) {
            $('#message-empty').css('display', 'block');
            $('#text1').css('display', 'none');
            $('#text2').css('display', 'none');
            $('#rte-pair').attr('data-pair-id', '');
            originalText1 = '';
            originalText2 = '';
        } else {
            $('#text1').text(data.t);
            $('#text2').text(data.h);
            $('#rte-pair').attr('data-pair-id', data.id);
            originalText1 = data.t;
            originalText2 = data.h;
        }
    }
    
    $.get(url, callback);
}

$(function(){
    $('#undo-button').click(function(){
        load_pair();
    });
});

$(function(){
    $('#ok-button').click(function(){
        url = '{{ url_for("clean_pair") }}';
        pair_id = $('#rte-pair').attr('data-pair-id');
        
        text1 = $('#text1').text();
        text2 = $('#text2').text();
        if (originalText1 != text1 || originalText2 != text2) {
            data = {operation: 'change', t: text1, h: text2, pair_id: pair_id};
        } else {
            data = {operation: 'none', pair_id: pair_id}
        }
        
        $.post(url, data, load_pair);
    });
});

$(function(){
    $('#discard-button').click(function(){
        url = '{{ url_for("clean_pair") }}';
        pair_id = $('#rte-pair').attr('data-pair-id');
        
        // make a POST request and load the next pair afterwards
        $.post(url, {operation: 'delete', pair_id: pair_id}, load_pair);
    });
});

