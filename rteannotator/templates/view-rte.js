function selectOption(element)
    {
        // send data to the server informing that this option was picked
        xmlhttp = new XMLHttpRequest();
        pairId = element.getAttribute('data-rte-id');
        classCode = element.getAttribute('data-rte-class');
        url = '{{ url_for('annotate_pair') }}' + '/' + pairId + '/' + classCode;
        
        xmlhttp.onreadystatechange = function()
        {
            divId = pairId + '-feedback';
            feedbackDiv = document.getElementById(divId);

            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                feedbackDiv.innerHTML = 'Annotation sent to server!';
                responseData = JSON.parse(xmlhttp.responseText);
                numAnnotatedDiv = document.getElementById('num_annotated');
                numAnnotatedDiv.innerHTML = responseData.num_annotated;
            }            
        }
        xmlhttp.open('GET', url, true);
        xmlhttp.send();
        
        // removes selected class from any other button for the same pair
        buttons = document.getElementsByName(element.name);
        var i;
        for (i = 0; i < buttons.length; i++)
        {
            button = buttons[i];
            button.className = button.className.replace(/(?:^|\s)selected(?!\S)/g , '' )
        }
        element.className = element.className + " selected";
    }
        
    $(function(){
        $('#hide-annotated').click(function(){
            // toggle annotation visibility
            if ($(this).is(':checked')) {
                url = '{{ url_for("toggle_show_all", switch=0) }}'
            } else {
                url = '{{ url_for("toggle_show_all", switch=1) }}'
            }
            document.location.href = url;
        });
    });
    
    /* function to make ajax request for the annotation */
    function callAnnotate(annotationType, button, value) {
        li = button.closest('li');
        pair_id = li.getAttribute('data-pair-id');
        url = '{{ url_for("annotate_pair") }}' + '/' + annotationType + '/' + pair_id + '/' + value;
        
        feedbackId = '#' + pair_id + '-' + annotationType + '-feedback';
        $(feedbackId).text('Sending...');
        
        callback = function(response, status) {
            data = JSON.parse(response);
            if (data.ok) {
                $(feedbackId).text('Sent!');
                
                // remove the "selected" class from sibling buttons
                $(button).siblings().removeClass('selected');
                $(button).addClass('selected');
            } else {
                $(feedbackId).text('Error sending data!');
            }
        }
        
        $.get(url, callback);
    };
    
    $(function(){
        $('.entailment-button').click(function(){
            button = this;
            value = this.getAttribute('data-rte-class');
            callAnnotate('entailment', button, value);
        })
    });
    
    $(function(){
        $('.similarity-button').click(function(){
            button = this;
            value = this.getAttribute('data-sim-class');
            callAnnotate('similarity', button, value);
        })
    });
    
    $(function(){
        $('.review-confirm').click(function(){
            rtePair =  $(this).parents('.rte-pair');
            texts = rtePair.find('.text-content');
            texts.attr('contenteditable', 'false');
                        
            // send POST request with new content
            url = '{{ url_for("edit_pair") }}';
            data = {t: rtePair.find('.pair1 .text-content').text(),
                    h: rtePair.find('.pair2 .text-content').text(),
                    pair_id: rtePair.attr('data-pair-id')};
            
            clickedButtton = this;
            callback = function(response, status){
                data = JSON.parse(response);
                if (data.ok) {
                    edit = $(clickedButtton).siblings('.review-edit');
                    edit.css('display', 'inline');
                    $(clickedButtton).css('display', 'none');
                } else {
                    alert('Could not edit pair');   
                }
            }
            
            $.post(url, data, callback);
        })
    });
    
    $(function(){
        $('.review-edit').click(function(){
            rtePair =  $(this).parents('.rte-pair');
            texts = rtePair.find('.text-content');
            texts.attr('contenteditable', 'true');
            
            confirm = $(this).siblings('.review-confirm');
            confirm.css('display', 'inline');
            $(this).css('display', 'none');
        })
    });
    
    $(function(){
        $('.review-delete').click(function(){
            
            parent = $(this).parents('[data-pair-id]');
            pair_id = parent.attr('data-pair-id');
            
            callback = function(response, status) {
                data = JSON.parse(response);
                if (data.ok) {
                    parent.fadeOut(800, function(){ parent.remove(); });
                }
            }
            
            $.post('{{ url_for("flag_pair") }}', {operation: 'delete', pair_id: pair_id}, callback=callback);
        })
    });