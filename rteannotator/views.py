# -*- coding: utf-8 -*-

import os
import json
import flask
import werkzeug
from flask import session, request
from flask.ext.login import login_required, login_user, logout_user, current_user

from rteannotator import model
from rteannotator import utils
from rteannotator import app

@app.route('/')
@login_required
def index():
    return flask.render_template('home.html')

@app.route('/submit', methods=['POST'])
@login_required
def submit():
    '''
    File submission for annotation
    '''
    file_ = request.files['file']
    filename = werkzeug.utils.secure_filename(file_.filename)
    
    # check if file exists in DB
    File = model.File
    file_in_db = File.query.filter_by(name=filename).first()
    if file_in_db is not None:
        flask.flash('There already exists a file with the same name. '\
                    'Are you sure this has not been submitted before?')
        return flask.redirect(flask.url_for('index'))
    
    path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    file_.save(path)
    model.parse_rte_file(path)
    return flask.redirect(flask.url_for('annotate_file'))

@app.route('/show-all')
@app.route('/show-all/<int:switch>')
@login_required
def toggle_show_all(switch=None):
    '''
    If switch is 1, set the annotator with the given id to see 
    all pairs, including the ones already annotated by him/her.
    
    If switch is 0, only unannotated pairs will be shown.
    '''
    if switch is None:
        # Ugly hack just to provide an endpoint with no arguments
        return ''
    
    username = session['username']
    if switch == 0:
        switch = False
    elif switch == 1:
        switch = True
    
    model.toggle_show_all_pairs(username, switch)
    return flask.redirect(flask.url_for('annotate_file'))

ITEMS_PER_PAGE = 5
    
@app.route('/choose-file', methods=['GET', 'POST'])
@login_required
def choose_file():
    filename = request.form['filename']
    if not utils.check_filename(filename): 
        return flask.render_template('forbidden.html')
    
    session['filename'] = filename
    return flask.redirect(flask.url_for('display_rte_data'))

@app.route('/review-file/', defaults={'page': None, 'file_id': None})
@app.route('/review-file/<int:file_id>/', defaults={'page': 1})
@app.route('/review-file/<int:file_id>/<int:page>')
@login_required
def review_file(file_id, page=1):
    '''
    Display previously annotated pairs.
    '''
    if file_id is None:
        return ''
    
    rte_file = model.File.query.get(file_id)
    rte_file.load_metadata()
    pair_query = model.get_pairs_file_query(file_id, True)
    pair_annotated_by_user_query = model.filter_annotations_by_user(pair_query)
    num_pairs = pair_annotated_by_user_query.count()
    paginated_query = utils.paginate_query(pair_query, page, ITEMS_PER_PAGE)
    num_pages = utils.find_num_pages(num_pairs, ITEMS_PER_PAGE)
     
    pairs = paginated_query.all()
    model.add_annotations_by_user(pairs)
    return flask.render_template('view-rte.html',
                                 page=page,
                                 file=rte_file,
                                 pairs=pairs,
                                 num_pages=num_pages,
                                 annotations_per_pair=model.DESIRED_ANNOTATIONS_PER_PAIR,
                                 showing_all=current_user.show_all_pairs)

@app.route('/annotate-file/', defaults={'page': None, 'file_id': None})
@app.route('/annotate-file/<int:file_id>', defaults={'page': 1})
@app.route('/annotate-file/<int:file_id>/<int:page>')
@login_required
def annotate_file(file_id, page):
    '''
    Display an RTE file for annotation.
    '''
    rte_file = model.File.query.get(file_id)
    rte_file.load_metadata()
    
    # get ITEMS_PER_PAGE pairs for this annotator
    pairs = model.get_pairs_for_annotation(file_id, ITEMS_PER_PAGE)
    
    model.add_annotations_by_user(pairs)
    
    return flask.render_template('view-rte.html',
                                 page=page,
                                 file=rte_file,
                                 pairs=pairs,
                                 annotations_per_pair=model.DESIRED_ANNOTATIONS_PER_PAIR,
                                 showing_all=current_user.show_all_pairs)

@app.route('/examples')
def examples():
    '''
    Show annotation examples
    '''
    return flask.render_template('examples.html')

# @app.route('/combine-files', methods=['GET', 'POST'])
# @login_required
# def combine_files():
#     '''
#     Combine the annotated data in the given files, providing a single
#     one to download.
#     '''
#     if request.method == 'GET':
#         return flask.render_template('choose-file.html',
#                                      filenames=utils.get_uploaded_files(),
#                                      action_url=flask.url_for('combine_files'),
#                                      multiselect=True)
#     
#     filenames = request.form.getlist('filenames[]')
#     all_pairs = []
#     for filename in filenames:
#         if not utils.check_filename(filename):
#             return flask.render_template('forbidden.html')
#         
#         full_path = os.path.join(utils.get_upload_folder(), filename)
#         file_data = utils.load_pairs_from_xml(full_path)
#         pairs = file_data.pairs
#         annotated_pairs = [pairs[num] for num in pairs
#                            if pairs[num].entailment != 'UNKNOWN']
#         all_pairs.extend(annotated_pairs)
#     
# #     file_data = model.FileData(None, all_pairs, len(all_pairs))
#     
#     tree = file_data.generate_xml()
#     combined_file = 'combined-rte.xml'
#     path = os.path.join(utils.get_upload_folder(), combined_file)
#     tree.write(path, 'utf-8', True)
#     
#     return flask.send_file(path, as_attachment=True,
#                            mimetype='text/plain')
    

@app.route('/statistics/', defaults={'file_id': None})
@app.route('/statistics/<int:file_id>')
@login_required
def statistics(file_id):
    file_ = model.File.query.get(file_id)
    
    stats = model.compute_file_statistics(file_id)
    metrics, entailment_counter, similarity_counter = stats
    
    return flask.render_template('statistics.html',
                                 filename=file_.name,
                                 entailment_counter=entailment_counter,
                                 similarity_counter=similarity_counter,
                                 metrics=metrics)

@app.route('/annotator-statistics', methods=['POST'])
@login_required
def annotator_statistics():
    '''
    Return a view with statistics on annotators's raw agreement.
    '''
    ids_string = request.form['ids']
    annotator_ids = [int(val) for val in json.loads(ids_string)]
    annotator_ids.sort()
    
    agreements = model.compute_annotators_statistics(annotator_ids)
    annotators = model.get_annotators(annotator_ids)
    for annotator, agreement in zip(annotators, agreements):
        annotator.agreement = agreement
    
    return flask.render_template('annotator-statistics.html', users=annotators)

@app.route('/overall-statistics', methods=['GET', 'POST'])
@login_required
def overall_statistics():
    '''
    Return a view with statistics over all pair annotations.
    '''
    if request.method == 'POST':
        # annotator ids were sent via post
        ids_string = request.form['ids']
        annotator_ids = json.loads(ids_string)
    else:
        annotator_ids = None
    
    try:
        stats = model.compute_overall_statistics(annotator_ids)
    except IndexError:
        if annotator_ids is not None:
            flask.flash('No pairs fully annotated by only the selected annotators.', 'error')
            return flask.redirect(flask.url_for('view_users'))
        else:
            flask.flash('No pairs fully annotated.', 'error')
            return flask.redirect(flask.url_for('view_files'))
        
    metrics, entailment_counter, similarity_counter = stats
    
    return flask.render_template('statistics.html',
                                 entailment_counter=entailment_counter,
                                 similarity_counter=similarity_counter,
                                 metrics=metrics)

@app.route('/annotate-pair')
@app.route('/annotate-pair/<annotation_type>/<pair_id>/<class_>')
@login_required
def annotate_pair(annotation_type=None, pair_id=None, class_=None):
    '''
    Receive entailment or similarity annotation. 
    '''
    if pair_id is None:
        # Ugly hack just to provide an endpoint with no arguments
        return ''
    
    annotator_id = current_user.id
    model.annotate_pair(annotation_type, pair_id, annotator_id, class_)
    
    response_data = {'ok': True}
    return json.dumps(response_data)

@app.route('/hints')
def hints():
    return flask.render_template('hints.html')

@app.route('/download/', defaults={'file_id': None})
@app.route('/download/<int:file_id>')
@login_required
def download_raw(file_id):
    if file_id is None:
        return ''
    
    file_ = model.File.query.get(file_id)
    file_data = model.generate_rte_file(file_id)
    return flask.send_file(file_data, as_attachment=True,
                           mimetype='text/xml',
                           attachment_filename=file_.name)

@app.route('/download-annotated/', methods={'POST'})
@login_required
def download_annotated():
    num_pairs = int(request.form['num-pairs'])
    
    all_files = bool(request.form.get('all-files'))
    if not all_files:
        file_id = int(request.form['file-id'])
        filename = model.File.query.get(file_id).name
        filename = filename.replace('.xml', '-annotated.xml')
    else:
        file_id = None
        filename = 'rte-annotated.xml'
    
    agreeing_annotations = int(request.form['agreeing'])
    
    file_data = model.generate_annotated_file(file_id, num_pairs, agreeing_annotations)
    return flask.send_file(file_data, as_attachment=True,
                           mimetype='text/xml',
                           attachment_filename=filename)
    

@app.route('/view-users', methods=['GET', 'POST'])
@login_required
def view_users():
    if request.method == 'GET':
        Annotator = model.Annotator
        users = Annotator.query.all()
        
        if current_user.may_see_all_statistics():
            # load number of annotations per user
            model.add_annotation_counts(users)
            
        return flask.render_template('view-users.html', users=users)
    
    # POST request
    username = request.form['username']
    password = request.form['password']
    model.create_user(username, password)
    return flask.redirect(flask.url_for('view_users'))

@app.route('/view-files')
@login_required
def view_files():
    '''
    Just show the available files and operations on them.
    '''
    File = model.File
    files = File.query.filter_by(available=True).all()
    for file_ in files:
        file_.load_metadata()
    
    return flask.render_template('choose-file.html', files=files)

@app.route('/get-pair/<int:file_id>')
@login_required
def get_pair_for_review(file_id):
    '''
    Get a pair from the specified file to be reviewed by a user
    '''
    username = session['username']
    pair = model.get_unreviewed_pair(username, file_id)
    if pair is None:
        response_data = {'empty': True}
    else:
        response_data = {'id': pair.id,
                         't': pair.t,
                         'h': pair.h}
    
    response_data['ok'] = True
    return json.dumps(response_data)

@app.route('/clean-file/', defaults={'file_id': None})
@app.route('/clean-file/<int:file_id>')
@login_required
def clean_file(file_id):
    if file_id is None:
        return ''
    
    return flask.render_template('clean.html', 
                                 file_id=file_id)

@app.route('/clean-pair', methods=['POST'])
@login_required
def clean_pair():
    '''
    Make a change in a pair.
    '''
    if not current_user.may_edit_pair():
        return json.dumps({'ok': False})
    
    annotator_name = current_user.name
    operation = request.form['operation']
    pair_id = request.form['pair_id']
    
    if operation == 'delete':
        model.delete_pair(annotator_name, pair_id)
    elif operation == 'change':
        t = request.form['t']
        h = request.form['h']
        model.change_pair(pair_id, t, h, True)
    else:
        model.mark_as_reviewed(annotator_name, pair_id)
    
    return json.dumps({'ok': True})

@app.route('/edit-pair', methods=['POST'])
@login_required
def edit_pair():
    '''Edit an existing pair, which was supposedly ready.'''
    if not current_user.may_edit_pair():
        return json.dumps({'ok': False})
    
    pair_id = request.form['pair_id']
    t = request.form['t']
    h = request.form['h']
    
    model.change_pair(pair_id, t, h, False)
    model.delete_annotations(pair_id)
    
    return json.dumps({'ok': True})

@app.route('/flag-pair', methods=['POST'])
@login_required
def flag_pair():
    '''
    Function called during annotation to flag a pair for either editing or deletion.
    '''
    operation = request.form['operation']
    pair_id = request.form['pair_id']
    result_ok = model.flag_pair(pair_id, operation)
    
    if result_ok:
        return json.dumps({'ok': True})
    else:
        flask.flash('Invalid operation', 'error')
        return json.dumps({'ok': False})

@app.route('/login', methods=['GET', 'POST'])
def login():
    
    if request.method == 'GET':
        return flask.render_template('login.html')
    
    username = request.form['username']
    password = request.form['password']
    session['username'] = username
    
    valid, msg = model.validate_login(username, password)
    if not valid:
        flask.flash(msg)
        return flask.render_template('login.html')
    
    annotator = model.find_annotator(username)
    login_user(annotator)
    
    # redirect to the next page, if there is one
    next_url = flask.request.args.get('next')
    if not next_url:
        return flask.redirect(flask.url_for('index'))
    
    return flask.redirect(next_url)

@app.route('/logout')
@login_required
def logout():
    session.pop('username', None)
    logout_user()
    return flask.redirect(flask.url_for('login'))

@app.route('/change-password', methods=['GET', 'POST'])
@login_required
def change_password():
    if request.method == 'GET':
        return flask.render_template('change-password.html')
    
    # POST
    old_password = request.form['old-password']
    new_password = request.form['new-password']
    if model.change_password(old_password, new_password):
        flask.flash('Password successfully changed', 'info')
        return flask.redirect(flask.url_for('index'))
    else:
        flask.flash('Wrong password', 'error')
        return flask.render_template('change-password.html')

def url_for_other_page(page):
    args = request.view_args.copy()
    args['page'] = page
    return flask.url_for(request.endpoint, **args)

app.jinja_env.globals['url_for_other_page'] = url_for_other_page
