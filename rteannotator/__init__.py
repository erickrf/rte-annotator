# -*- coding: utf-8 -*-

'''
Definition of the Flask application object
'''

import logging
import os
import itertools

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
import jinja2

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

@app.template_global(name='zip_longest')
def _zip(*args, **kwargs):
    return itertools.izip_longest(*args, **kwargs)

login_manager = LoginManager()
login_manager.init_app(app)

if not app.debug:
    
    dir_name = os.path.dirname(__file__)
    file_path = os.path.join(dir_name, '..', 'errors.log')
    file_handler = logging.FileHandler(file_path)
    file_handler.setLevel(logging.WARNING)
    
    formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s '\
                                  '[in %(pathname)s:%(lineno)d]')
    file_handler.setFormatter(formatter)

    app.logger.addHandler(file_handler)

import views

login_manager.login_view = 'login'
